<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           category_tag_import_export_for_woocommerce
 *
 * @wordpress-plugin
 * Plugin Name:       Category Tag Import Export for Woocommerce
 * Plugin URI:        
 * Description:       Category Tag Import Export for Woocommerce in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Mozilor
 * Author URI:        
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       category_tag_import_export_for_woocommerce
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_category_tag_import_export_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-category-tag-import-export-for-woocommerce-activator.php';
	category_tag_import_export_for_woocommerce_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_category_tag_import_export_for_woocommerce() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-category-tag-import-export-for-woocommerce-deactivator.php';
	category_tag_import_export_for_woocommerce_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_category_tag_import_export_for_woocommerce' );
register_deactivation_hook( __FILE__, 'deactivate_category_tag_import_export_for_woocommerce' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-category-tag-import-export-for-woocommerce.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_category_tag_import_export_for_woocommerce() {

	$plugin = new category_tag_import_export_for_woocommerce();
	$plugin->run();

}
run_category_tag_import_export_for_woocommerce();
