(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

})( jQuery );
function disableTagColoum() {

    document.getElementById('columns[thumbnail]').checked = false;
    document.getElementById('thumbnail').style.display = "none";

    document.getElementById('columns[parent]').checked = false;
    document.getElementById('parent').style.display = "none";
}

function enableAllColoum() {

    document.getElementById('columns[thumbnail]').checked = true;
    document.getElementById('thumbnail').style.display = "block";

    document.getElementById('columns[parent]').checked = true;
    document.getElementById('parent').style.display = "block";
}

function enableColoumforPostcat() {

    document.getElementById('columns[thumbnail]').checked = false;
    document.getElementById('thumbnail').style.display = "none";

    document.getElementById('columns[parent]').checked = true;
    document.getElementById('parent').style.display = "block";
}


jQuery(document).ready(function(a) {
    "use strict";
    // Listen for click on toggle checkbox
    jQuery('#pselectall').live("click", function() {
        // Iterate each checkbox
        jQuery(':checkbox').each(function() {
            this.checked = true;
        });
    });
    jQuery('#punselectall').live("click", function() {
        // Iterate each checkbox
        jQuery(':checkbox').each(function() {
            this.checked = false;
        });
    });
});

function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        var _validFileExtensions = document.querySelector('input[name="import_as"]:checked').value; 
        // alert(sFileName);
         if (sFileName.length > 0) {
            var blnValid = false;
           if(sFileName.includes(_validFileExtensions))
           {
            blnValid = true;
           }

            if (!blnValid) {
                alert("Please upload valid file");
                oInput.value = "";
                // location.reload();
                return false;
            }
        }
    }
    return true;
}