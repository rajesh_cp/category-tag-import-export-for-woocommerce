<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class category_tag_import_export_for_woocommerce_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}




public function admin_menu_page_settings() {

	add_submenu_page(
'woocommerce',
'ImporterExporter',
'Category Tag Import Export',
'manage_options',
'category_tag_import_export_for_woocommerce',
array( $this,  'render' )
);

add_submenu_page(
	'edit.php',
	'ImporterExporter',
	'Category Tag Import Export',
	'manage_options',
	'category_tag_import_export_for_woocommerce',
	array( $this,  'render' )
	);

    	// add_menu_page( 
     //    'My catagry listing admin menu', 
     //    'Importer / Exporter',
     //     'manage_options', 
     //     'category_tag_import_export_for_woocommerce', 
     //     array( $this, 'render' ), 
     //     'dashicons-admin-generic',7  );

    }


/*
callback function

*/
   
    function render() {

	   	 global $pagenow;

	   	 
	   	 $post_columns=array(
	    'name' => 'name',
	    'slug' => 'slug',
	    'description' => 'description',
	    'term_id' => 'term_id',
	    'parent' => 'parent',
	    'thumbnail' => 'thumbnail',
	    
		);
	 	require_once('partials/adMenu-admin-tab-content-display.php');
	   	 // echo "hai";

	}


    public static function wrap_column($data) {
        return '"' . str_replace('"', '""', $data) . '"';
    }


	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/category-tag-import-export-for-woocommerce-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/class-category-tag-import-export-for-woocommerce-admin.js', array( 'jquery' ), $this->version, false );

	}

}
