<script>
function resize() {
    var redi = document.querySelector('input[name="category_export"]:checked').value;
    if (redi == "Post") {

        document.getElementById('thumbnail').style.display = "none";
        document.getElementById('columns[thumbnail]').checked = false;
    }
    if (redi == "Tag_product" || redi == "Tag_post") {

        document.getElementById('thumbnail').style.display = "none";
        document.getElementById('columns[thumbnail]').checked = false;

        document.getElementById('parent').style.display = "none";
        document.getElementById('columns[parent]').checked = false;
    }


}

window.onload = resize;
</script>

<div class="outer">
    <div class="inner">
        <h3 class="title">
            <?php _e('Export Category/Tag in CSV or XML Format:', 'category-tag-import-export-for-woocommerce'); ?></h3>
        <hr>
        <p><?php _e('Export and download your Category/Tag in CSV or XML format.', 'category-tag-import-export-for-woocommerce'); ?>
        </p>
        <?php 
    $url=admin_url().'admin.php?&page=category_tag_import_export_for_woocommerce&tab=export_action';
  ?>
        <form method="post" action=<?php echo $url;?>>
            <table class="form-table" id="griddata">

                <tr>

                    <th>
                        <label
                            for="exoprt_to"><?php _e(' Choose what to export', 'category-tag-import-export-for-woocommerce'); ?></label>
                    </th>
                    <td>
                    <?php 
                        if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
                    ?>
                        <input name="category_export" onclick="enableAllColoum()" type="radio" value="Product"
                            id="Product_Category" />
                        <label
                            for="Product_Category"><?php _e('Product Category', 'category-tag-import-export-for-woocommerce'); ?></label>&nbsp&nbsp&nbsp
                        <input name="category_export" onclick="disableTagColoum()" type="radio" value="Tag_product"
                            id="Tag_product" />
                        <label
                            for="Tag_product"><?php _e('Product Tag', 'category-tag-import-export-for-woocommerce'); ?></label>
                        &nbsp&nbsp&nbsp
                    <?php }
                     ?> 
                        <input name="category_export" onclick="enableColoumforPostcat()" type="radio" value="Post"
                            id="Post_Category" checked>
                        <label
                            for="Post_Category"><?php _e('Post Category', 'category-tag-import-export-for-woocommerce'); ?></label>
                        &nbsp&nbsp&nbsp
                        <input name="category_export" onclick="disableTagColoum()" type="radio" value="Tag_post"
                            id="Tag_post" />
                        <label
                            for="Tag_post"><?php _e('Post Tag', 'category-tag-import-export-for-woocommerce'); ?></label>

                    </td>

                </tr>
                <tr>
                    <th>
                        <label
                            for="exoprt_to"><?php _e('Export formate', 'category-tag-import-export-for-woocommerce'); ?></label>
                    </th>
                    <td>
                        <input id="csv_export" name="export-to" type="radio" value="csv" checked>
                        <label
                            for="csv_export"><?php _e('CSV', 'category-tag-import-export-for-woocommerce'); ?></label>&nbsp&nbsp&nbsp
                        <input id="xml_export" name="export-to" type="radio" value="xml">
                        <label
                            for="xml_export"><?php _e('XML', 'category-tag-import-export-for-woocommerce'); ?></label>

                    </td>
                </tr>
                <tr>

                    <table id="datagrid">
                        <!-- <th style="text-align: left;">
                            <label
                                for="v_columns"><?php _e('Column', 'category-tag-import-export-for-woocommerce'); ?></label>
                        </th>
                         <th style="text-align: left;">
                    <label for="v_columns_name"><?php _e('Column Name', 'category-tag-import-export-for-woocommerce'); ?></label>
                </th> -->
                        <!-- select all boxes -->
                        <tr>
                            <td style="padding: 10px;">
                                <a href="#" id="pselectall"
                                    onclick="return false;"><?php _e('Select all', 'product-import-export-for-woo'); ?></a>
                                &nbsp;/&nbsp;
                                <a href="#" id="punselectall"
                                    onclick="return false;"><?php _e('Unselect all', 'product-import-export-for-woo'); ?></a>
                            </td>
                        </tr>
                        <?php foreach ($post_columns as $pkey => $pcolumn) { ?>
                        <tr id=<?php echo $pkey; ?>>
                            <td>
                                <input id="columns[<?php echo $pkey; ?>]" name="columns[<?php echo $pkey; ?>]"
                                    type="checkbox" value="<?php echo $pkey; ?>" checked />
                                <label
                                    for="columns[<?php echo $pkey; ?>]"><?php _e($pcolumn, 'category-tag-import-export-for-woocommerce'); ?></label>
                            </td>
                            <!-- <td>
                        <input type="text" name="columns_name[<?php echo $pkey; ?>]"
                            id="columns_name[<?php echo $pkey; ?>]" value="<?php echo $pkey; ?>" class="input-text" />
                    </td> -->
                        </tr>
                        <?php } ?>
                    </table><br />
                </tr>
            </table>
            <input name="download" value="true" type="hidden">
            <input type="submit" class="button button-primary" name="submit" value="Export">
        </form>
    </div>
</div>
<?php
    /**
 * receive value here
 * using isset($_POST['submit'])
 */
                if (isset($_POST['submit'])){
                  
  
                    
                }

                ?>
<script>

</script>

<script>

</script>