<div class="outer">
    <div class="inner">
        <div class="tool-box bg-white p-20p pipe-view">
            <h3 class="title">
                <?php _e('Import Category/Tag From CSV or XML Format:', 'category-tag-import-export-for-woocommerce'); ?>
            </h3>
            <hr>
            <p><?php _e('Import Category/Tag from CSV or XML format from your computer.You can import Category/Tag (in CSV or XML format) in to the shop.', 'category-tag-import-export-for-woocommerce'); ?>
            </p>
            <?php 
            $url=admin_url().'admin.php?&page=category_tag_import_export_for_woocommerce&tab=import_action';
            ?>

            <form enctype="multipart/form-data" action=<?php echo $url;?> id="import-upload-form" method="post">

                <table class="form-table">
                    <tbody>
                        <tr>
                            <th>
                                <label
                                    for="import-from"><?php _e('Choose what to import', 'category-tag-import-export-for-woocommerce'); ?></label>
                            </th>
                            <td>
                            <?php 
                            if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
                            ?>
                                <input id="Product_Categorys" name="categorys_import" type="radio" value="Product" />
                                <label
                                    for="Product_Categorys"><?php _e('Product Category', 'category-tag-import-export-for-woocommerce'); ?></label>&nbsp&nbsp&nbsp
                                    <input id="Tag_product" name="categorys_import" type="radio" value="Tag_product" />
                                <label
                                    for="Tag_product"><?php _e('Product Tag', 'category-tag-import-export-for-woocommerce'); ?></label>
                                &nbsp&nbsp&nbsp
                            <?php } ?>
                                <input id="Post_Category" name="categorys_import" type="radio" value="Post" checked>
                                <label
                                    for="Post_Category"><?php _e('Post Category', 'category-tag-import-export-for-woocommerce'); ?></label>
                                &nbsp&nbsp&nbsp
                                <input id="Tag_post" name="categorys_import" type="radio" value="Tag_post" />
                                <label
                                    for="Tag_post"><?php _e('Post Tag', 'category-tag-import-export-for-woocommerce'); ?></label>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label
                                    for="import-from"><?php _e('Import formate', 'category-tag-import-export-for-woocommerce'); ?></label>
                            </th>
                            <td>
                                <input id="csv_import" name="import_as" type="radio" value="csv" checked>
                                <label
                                    for="csv_import"><?php _e('CSV', 'category-tag-import-export-for-woocommerce'); ?></label>&nbsp&nbsp&nbsp
                                <input id="xml_import" name="import_as" type="radio" value="xml">
                                <label
                                    for="xml_import"><?php _e('XML', 'category-tag-import-export-for-woocommerce'); ?></label>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label
                                    for="import-from"><?php _e('Update data if exists', 'category-tag-import-export-for-woocommerce'); ?></label>
                            </th>
                            <td>
                                <input type="checkbox" name="merge" id="merge" value="update">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label
                                    for="upload"><?php _e('Select a file from your computer', 'category-tag-import-export-for-woocommerce'); ?></label>
                            </th>
                            <td>
                                <input type="file"onchange="ValidateSingleInput(this);"  id="upload" name="import" size="25" />
                                <?php
                            $bytes = apply_filters('import_upload_size_limit', wp_max_upload_size());
                            $size = size_format($bytes);
                            $upload_dir = wp_upload_dir();
                            ?>
                                <small><?php printf(__('Maximum size: %s'), $size); ?></small>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="submit" class="button button-primary" name="submit" value="Import">
            </form>

        </div>
    </div>
</div>


<?php	
	?>