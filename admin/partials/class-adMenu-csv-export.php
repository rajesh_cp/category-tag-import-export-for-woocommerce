<?php
class adMenu_csv_export {

	public function Export_to_csv($terms, $user_columns_name, $export_columns, $csv_columns, $type_export)
	{
         global $wpdb;
        $wpdb->hide_errors();
       @set_time_limit(0);
       if (function_exists('apache_setenv'))
           @apache_setenv('no-gzip', 1);
       @ini_set('zlib.output_compression', 0);
       @ob_end_clean();

        header('Content-Type: text/csv; charset=UTF-8');
        header('Content-Disposition: attachment; filename=Cat-exp-' . date('Y_m_d_H_i_s', current_time('timestamp')) . ".csv");
        header('Pragma: no-cache');
        header('Expires: 0');
        $fp = fopen('php://output', 'w');


        $row = array();
        // Export header rows
        foreach ($csv_columns as $column => $value) {
           @$temp_head = esc_attr($user_columns_name[$column]);
            if (!$export_columns || in_array($column, $export_columns))
                $row[] = $temp_head;
        }

       $row = array_map('category_tag_import_export_for_woocommerce::wrap_column', $row);
        fwrite($fp, implode(',', $row) . "\n");
        unset($row);


         if($type_export=='Tag_post' || $type_export=='Tag_product' || $type_export=='Post' || $type_export=='Product' ){
      
     foreach ($terms as $term) {

	if($type_export=='Product') {
	    $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
        $thumbnail = wp_get_attachment_url( $thumbnail_id );
        $term->thumbnail=$thumbnail;
	}
        $customer_data = array();
        foreach ($export_columns as $key) {

            $customer_data[$key] = !empty($term->{$key}) ? maybe_serialize($term->{$key}) : '';
        }
           
         $row = array_map('category_tag_import_export_for_woocommerce::wrap_column', $customer_data);
            // print_r($row);die;
             fwrite($fp, implode(',', $row) . "\n");
             unset($row);
            unset($customer_data);
}

} 
 
        fclose($fp);
        exit;   



	}

}