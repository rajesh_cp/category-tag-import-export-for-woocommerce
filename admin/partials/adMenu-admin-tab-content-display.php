<div class="display_title">

    <div id="icon-themes" class="icon32"></div>
    <h2> <?php _e('Category Tag Import Export', 'category-tag-import-export-for-woocommerce'); ?></h2>

    <?php
            if( isset( $_GET[ 'tab' ] ) ) {
                $active_tab = $_GET[ 'tab' ];
            } else{
                $active_tab = 'selective_export';
            }

            if($_GET['tab'] =='import_action'){
                $active_tab='import';
            }
    ?>

    <h2 class="nav-tab-wrapper">

        <a href="?page=category_tag_import_export_for_woocommerce&tab=selective_export"
            class="nav-tab <?php echo $active_tab == 'selective_export' ? 'nav-tab-active' : ''; ?>"><?php _e('Category/Tag', 'category-tag-import-export-for-woocommerce'); ?></a>
        <!-- <a href="?page=category_tag_import_export_for_woocommerce&tab=cat"
            class="nav-tab <?php echo $active_tab == 'cat' ? 'nav-tab-active' : ''; ?>">Catagory</a> -->
        <!-- <a href="?page=category_tag_import_export_for_woocommerce&tab=tag"
            class="nav-tab <?php echo $active_tab == 'tag' ? 'nav-tab-active' : ''; ?>">Tag</a> -->
        <a href="?page=category_tag_import_export_for_woocommerce&tab=export"
            class="nav-tab <?php echo $active_tab == 'export' ? 'nav-tab-active' : ''; ?>"><?php _e('Export', 'category-tag-import-export-for-woocommerce'); ?></a>
        <a href="?page=category_tag_import_export_for_woocommerce&tab=import"
            class="nav-tab <?php echo $active_tab == 'import' ? 'nav-tab-active' : ''; ?>"><?php _e('Import', 'category-tag-import-export-for-woocommerce'); ?></a>

    </h2>

    <?php


         if ( isset ( $_GET['tab'] ) )
            $tab = $_GET['tab'];
         else
           $tab = 'selective_export';

        
        switch ( $tab ){

          case 'export' :
                         require_once('adMenu-admin-export-cat-display.php');
                              break;   

            case 'import' :
                         require_once('adMenu-admin-import-cat-display.php');
                              break; 
             case 'import_action' :
             require_once('class-adMenu-admin-import-action.php');
            $actions= new adMenu_admin_import_action;
            $actions->Import_Action();
                              break; 
            case 'export_action' :
             require_once('class-adMenu-admin-export-action.php');
            $actions= new adMenu_admin_export_action;
            $actions->Export_Action();
                              break;
            case 'selective_export' : 
        
     
                         require_once 'My_Export_List_Table.php';
 
?>

    <form method="GET">
        <div class="exprt_list_table">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php
        $myListTable = new My_Export_List_Table();
        $myListTable->prepare_items(); 
        $myListTable->display();
        ?>
    </form>
</div>
<?php
   } 
    ?>

</div>