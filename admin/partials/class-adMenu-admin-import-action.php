<?php

class adMenu_admin_import_action {


public function Import_Action() {

   $import_as = !empty($_POST['import_as']) ? $_POST['import_as'] : '';

   $chk_existing_update = !empty($_POST['merge']) ? $_POST['merge'] : 'not_update';
	//  $chk_existing_update = !empty($_POST['category_import']) ? $_POST['category_import'] : '';
   $option_value = !empty($_POST['categorys_import']) ? $_POST['categorys_import'] : '';
  
     global $woocommerce, $wpdb;
     $this->delimiter='';
		 if (!$this->delimiter)
            $this->delimiter = ',';

           if (empty($_POST['file_url'])) {

            $file = wp_import_handle_upload();
            $this->id = (int) $file['id'];         
        } 
        
        if ($this->id)
            $file = get_attached_file($this->id);
          
          if(isset($file['error'])){
            $url=admin_url().'admin.php?&page=category_tag_import_export_for_woocommerce&tab=import';
            wp_redirect( $url );
            exit;
          }
         
     
	   // Set locale
        $enc = mb_detect_encoding($file, 'UTF-8, ISO-8859-1', true);
        if ($enc)
            setlocale(LC_ALL, 'en_US.' . $enc);
        @ini_set('auto_detect_line_endings', true);
          if($import_as == 'xml')
   {
    $table_display= $this->export_to_xml_file($file,$chk_existing_update,$option_value);
    // require('adMenu-admin-import-xml.php');
   }else{

        // Get headers
        if (( $handle = fopen($file, "r") ) !== FALSE) {

            $row = $raw_headers = array();
            $header = fgetcsv($handle, 0, $this->delimiter);
            while (( $postmeta = fgetcsv($handle, 0, $this->delimiter) ) !== FALSE) {
                foreach ($header as $key => $heading) {
                    if (!$heading)
                        continue;
                    $s_heading = $heading;
                    $row[$s_heading] = ( isset($postmeta[$key]) ) ? $this->format_data_from_csv($postmeta[$key], $enc) : '';
                    $raw_headers[$s_heading] = $heading;
                }
                break;
            }
            
            fclose($handle);
        }


        $file = str_replace("\\", "/", $file);
    
       $start_pos=0;
         $enc = mb_detect_encoding( $file, 'UTF-8, ISO-8859-1', true );
		if ( $enc )
			setlocale( LC_ALL, 'en_US.' . $enc );
		@ini_set( 'auto_detect_line_endings', true );

		$parsed_data = array();
		$raw_headers = array();
		// Put all CSV data into an associative array
		if ( ( $handle = fopen( $file, "r" ) ) !== FALSE ) {

			$header   = fgetcsv( $handle, 0, $this->delimiter );
		    while ( ( $postmeta = fgetcsv( $handle, 0, $this->delimiter ) ) !== FALSE ) {
	            $row = array();
				
	            foreach ( $header as $key => $heading ) {
					$s_heading = strtolower($heading);

					$row[$s_heading] = ( isset( $postmeta[$key] ) ) ? $this->format_data_from_csv( $postmeta[$key], $enc ) : '';

					$raw_headers[ $s_heading ] = $heading;
	            }
	            $parsed_data[] = $row;

	            unset( $postmeta, $row );
	             $position = ftell( $handle );

	      
		    }
		    fclose( $handle );
		}
    
$category_details = array();
$table_display = array();
                foreach ($parsed_data as $value) {

                	$satus=$this->create_category($value,$chk_existing_update,$option_value);
                	$table_display[]=$satus;
       
                }
$table_display= @array_filter(array_map('array_filter', $table_display));
              }
                ?>
<br><br>



<table id="import-progress" class="widefat_importer widefat">
    <thead>
        <tr>
            <th class="status">&nbsp;</th>
            <th class="row">Row</th>
            <th class="reason">Category Name</th>
            <!-- <th>User Status</th> -->
            <th class="reason">Status Msg</th>
        </tr>
    </thead>
    <?php
        	$i=0;
          foreach ( $table_display as $status ) {
            ?>
    <tr>
        <td></td>
        <td class="row"><?php echo ++$i; ?></td>
        <td class="reason"><?php echo $status['name']; ?></td>
        <td class="reason"><?php echo $status['status']; ?></td>

    </tr>
    <?php
                                   } 
                                   ?>
    </tr>
    </tbody>
</table>
<?php 
$url=admin_url().'admin.php?page=category_tag_import_export_for_woocommerce&tab=import';
            ?>
<a href=<?php echo $url;?>>Back</a>

<?php 



}

   

/**
 * 
 * utf8 encoding functon
 * 
 */


public function format_data_from_csv($data, $enc) {
        return ( $enc == 'UTF-8' ) ? $data : utf8_encode($data);
    }


/**
 * 
 * this function used to store data in db
 * correct formated data and options are receive 
 * status return
 * 
 */


public function create_category($data,$chk_existing_update,$option_value) {
	 	$name=$data['name'];
	 	$slug=$data['slug'];
     $term_id=$data['term_id'];
    if(isset($data['parent']))
        $parents=$data['parent'];
                  global $wpdb;

                        switch ($option_value) {

                        case 'Product':
                            $tax_type='product_cat';
                            $term_meta_tbl_key='orginal_term_id';
                            break;
                        case 'Post':
                            $tax_type='category';
                            $term_meta_tbl_key='orginal_post_cat_term_id';
                            break;
                        case 'Tag_product':
                              $tax_type='product_tag';
                              $term_meta_tbl_key='orginal_product_tag_term_id';
                              break;
                        case 'Tag_post':
                              $tax_type='post_tag';
                              $term_meta_tbl_key='orginal_post_tag_term_id';
                              
                            break;
                      
                    } 
                $pid='';
                if(isset($data['parent'])){
                  $pid= $data['parent'];

                }
              $term_name=$data['name'];
              $taxonomy_name=$tax_type;
              $related_data=array(
                'name' =>$data['name'],
                'description'=> $data['description'],
                'slug' => $data['slug']);
              $related_data['parent']=$pid;
            $chk =  $wpdb->get_results( $wpdb->prepare( "SELECT t.term_id, t.name, t.slug FROM $wpdb->terms AS t INNER JOIN $wpdb->term_taxonomy as tt ON tt.term_id = t.term_id WHERE t.name = %s and t.slug = %s and tt.taxonomy = %s ORDER BY t.term_id", $name, $slug, $tax_type ), ARRAY_A );

            $tid=''; $status='';
            if(isset($chk[0]['term_id']))
            $tid=$chk[0]['term_id'];

  if($option_value=='Tag_product' || $option_value=='Tag_post' || $option_value=='Post' || $option_value=='Product'){

            if($tid=='') 
            {

                if(!empty($data['slug'])) 
            {

              if($option_value=='Post' || $option_value=='Product' ){

                $res =  $wpdb->get_results( $wpdb->prepare( "SELECT term_id FROM $wpdb->termmeta WHERE meta_key = %s and meta_value = %d ORDER BY meta_key,meta_id", $term_meta_tbl_key, $parents ), ARRAY_A );
                
              $term_id=$data['term_id'];
              
            if(!empty($res)) 
            {
              $pid= $res[0]['term_id'];
              
            }
            $related_data['parent']=$pid;
              }


              if(!empty($parents) && empty($res) ){

                $status = array(
                  'name' => $data['name'],
                  'status' => 'Data skipped parent not found',
                
              );

              }else{
              
              $cid = wp_insert_term($term_name,$taxonomy_name,$related_data);

            $cid=$cid['term_id'];
            update_term_meta( $cid, $term_meta_tbl_key , $term_id );

            if($option_value=='Product'){
              
              $thumbnail= @$data['thumbnail'];
            if($thumbnail!=""){
              
            $image_url = $data['thumbnail'];
            $attach_id = $this->image_library_attachment($image_url);
                update_term_meta( $cid, 'thumbnail_id', absint( $attach_id ) );
            }
            }
              $status = array(
                        'name' => $data['name'],
                        'status' => 'imported',
                      
                    );
            unset($cid);


                  }
            }
            }else{
                    if($chk_existing_update=='update'){
                        $update = wp_update_term( $tid, $taxonomy_name, $related_data);
                        if($option_value=='Product')
                        {
                          $thumbnail= $data['thumbnail'];
                        if($thumbnail!=""){

                          $thumbnail_id = get_term_meta( $tid, 'thumbnail_id', true );
                          $thumbnail_url = wp_get_attachment_url( $thumbnail_id );
                          $existing_filename = basename( $thumbnail_url );

                    $image_url = $data['thumbnail'];
                    $current_filename = basename( $image_url );
                    if($current_filename!=$existing_filename){
                    
                      $attach_id = $this->image_library_attachment($image_url);

                        update_term_meta( $tid, 'thumbnail_id', absint( $attach_id ) );
                    }
                    }

                        }

                        $status = array(
                                'name' => $data['name'],
                                'status' => 'updated',
                              
                            );
                      

                      }else{
                        $status = array(
                                'name' => $data['name'],
                                'status' => 'exist',
                              
                            );
                        }
                $term_id=$data['term_id'];
                update_term_meta( $tid, $term_meta_tbl_key , $term_id );

              }

  }

   unset($chk);
   return $status ;
   
}


/**
 * Method used for attach image file to wp library
 *  
 * $image_url is url
 * return attachment id
 */

public function image_library_attachment($image_url) {

  $upload_dir = wp_upload_dir();

$image_data = file_get_contents( $image_url );

$filename = basename( $image_url );

if ( wp_mkdir_p( $upload_dir['path'] ) ) {
  $file = $upload_dir['path'] . '/' . $filename;
}
else {
  $file = $upload_dir['basedir'] . '/' . $filename;
}

file_put_contents( $file, $image_data );

$wp_filetype = wp_check_filetype( $filename, null );

$attachment = array(
  'post_mime_type' => $wp_filetype['type'],
  'post_title' => sanitize_file_name( $filename ),
  'post_content' => '',
  'post_status' => 'inherit'
);

$attach_id = wp_insert_attachment( $attachment, $file );
require_once( ABSPATH . 'wp-admin/includes/image.php' );
$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
wp_update_attachment_metadata( $attach_id, $attach_data );

 return $attach_id ;

}



public function export_to_xml_file($file,$chk_existing_update,$option_value) {
  require_once 'rss_php.php';
   $rss = new rss_php;

    $rss->load($file);

    $items = $rss->getItems(); #returns all rss items
    $data =$items['rss']['channel']['data'];
     foreach ($data as $value1) {

     	    foreach ($value1 as $key => $value) {
				  if(is_array( $value) and  $value['value']== ''){
				    $new_array[$key] = '';
				  }else{
				    $new_array[$key] = $value;
				  }
				}
     	      
                	$satus=$this->create_category($new_array,$chk_existing_update,$option_value);
                	$table_display[]=$satus;
       
                }

                return $table_display;

              
}
}